package cn.basttg.core.controller;

import cn.basttg.core.exception.BusinessException;
import cn.basttg.core.exception.ParameterException;
import cn.basttg.core.service.TestService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * 测试异常
 * 参考 https://blog.csdn.net/ufo2910628/article/details/40399539
 *
 * @author pengj
 * 2018-05-13 下午11:49:35
 */
@Controller
public class TestController {//extends BaseController {

    @Resource
    private TestService testService;

    @RequestMapping(value = "/controller.do", method = RequestMethod.GET)
    public void controller(HttpServletResponse response, Integer id) throws Exception {
        System.out.println("收到id=" + id);
        switch (id) {
            case 1:
                throw new BusinessException("10", "controller10");
            case 2:
                throw new BusinessException("20", "controller20");
            case 3:
                throw new BusinessException("30", "controller30");
            case 4:
                throw new BusinessException("40", "controller40");
            case 5:
                throw new BusinessException("50", "controller50");
            default:
                throw new ParameterException("Controller Parameter Error");
        }
    }

    @RequestMapping(value = "/service.do", method = RequestMethod.GET)
    public void service(HttpServletResponse response, Integer id) throws Exception {
        testService.exception(id);
    }

    @RequestMapping(value = "/dao.do", method = RequestMethod.GET)
    public void dao(HttpServletResponse response, Integer id) throws Exception {
        testService.dao(id);
    }
}